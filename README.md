# Reproducible data science: what and why?

I prepared this talk for the Master of Data Science cohort on Friday 3 May, 2024.

See the [PDF version of the slides](moss-reproducible-data-science.pdf).
